# Getting Started with Spring Boot


_tbd_


## References

* [Spring Boot](https://projects.spring.io/spring-boot/)
* [String Initializr](https://start.spring.io/)
* [Installing the Spring Boot CLI](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#getting-started-installing-the-cli)
* [Spring Boot Getting Started](https://spring.io/guides/gs/spring-boot/)
* [Spring Boot Reference Guide](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
* [spring-projects/spring-boot](https://github.com/spring-projects/spring-boot)
* []()
* []()

